# NO. 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

![Gif.1](https://gitlab.com/1217050064/project-brimo/-/raw/main/Nomer1.gif)

# NO. 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Jawaban :

Algoritma Login dengan Penyimpanan Kata Sandi (Password-based Login):

Pengguna memasukkan email dan kata sandi.
Sistem membandingkan kata sandi yang dimasukkan dengan kata sandi yang disimpan dalam basis data atau penyimpanan yang sesuai.
Jika kata sandi cocok, pengguna dianggap berhasil login.
Contoh algoritma hashing yang umum digunakan untuk penyimpanan kata sandi adalah BCrypt atau PBKDF2.

![Gif.2](https://gitlab.com/1217050064/project-brimo/-/raw/main/Nomer2.gif)

# NO. 3
Mampu menjelaskan konsep dasar OOP

Jawaban :
Berikut adalah konsep dasar OOP yang diterapkan:

Kelas: Program tersebut menggunakan kelas-kelas untuk mengorganisir kode dan data terkait. Terdapat kelas-kelas seperti Dashboard, Transfer, Pembayaran, dan TopUpPulsa. Setiap kelas ini mencerminkan entitas atau objek yang terkait dengan fungsionalitas aplikasi.

Objek: Objek-objek diciptakan dari kelas-kelas yang telah didefinisikan. Misalnya, objek dashboard dibuat dari kelas Dashboard dan objek transfer dibuat dari kelas Transfer. Setiap objek memiliki state (data) dan perilaku (metode) yang terkait.

Enkapsulasi: Enkapsulasi melibatkan pengelompokkan data dan metode terkait dalam sebuah objek dan memberikan akses yang terkontrol. Misalnya, variabel saldo dalam kelas Dashboard dinyatakan sebagai private, sehingga hanya dapat diakses melalui metode-metode yang ada dalam kelas tersebut.

Pewarisan (Inheritance): Pewarisan memungkinkan kelas untuk mewarisi sifat-sifat (data dan metode) dari kelas lain. Misalnya, kelas Transfer, Pembayaran, dan TopUpPulsa mungkin mewarisi sifat-sifat umum dari kelas Transaksi.

Polimorfisme: Polimorfisme memungkinkan objek dari kelas yang berbeda untuk merespons metode yang sama dengan cara yang berbeda. Dalam program di atas, jika terdapat metode yang sama di kelas-kelas turunan, objek-objek tersebut dapat digunakan secara polimorfik.

Abstraksi: Abstraksi melibatkan pemodelan objek dan fungsionalitas yang penting secara konseptual, sementara detail implementasi diabaikan. Dalam program di atas, kelas-kelas seperti Transfer, Pembayaran, dan TopUpPulsa mewakili abstraksi dari fungsi-fungsi transfer, pembayaran, dan top up pulsa.

![Gif.3](https://gitlab.com/1217050064/project-brimo/-/raw/main/Nomer3.gif)

# NO. 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

![Gif.4](https://gitlab.com/1217050064/project-brimo/-/raw/main/Nomer4.gif)

# NO. 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)

![Gif.5](https://gitlab.com/1217050064/project-brimo/-/raw/main/Nomer5.gif)

# NO. 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

![Gif.6](https://gitlab.com/1217050064/project-brimo/-/raw/main/Nomer6.gif)

# NO. 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Identifikasi objek utama: Analisislah proses bisnis dan identifikasi objek-objek utama yang terlibat. Objek-objek ini dapat berupa entitas, konsep, atau aktor dalam proses bisnis. Misalnya, dalam sistem perbankan, objek-objek utama bisa termasuk Akun, Transaksi, Nasabah, dan lain sebagainya.

Tentukan atribut dan metode objek: Untuk setiap objek, tentukan atribut yang merepresentasikan data yang relevan dan metode yang menggambarkan perilaku atau tindakan yang dapat dilakukan oleh objek tersebut. Misalnya, objek Akun dapat memiliki atribut seperti nomor rekening, saldo, dan pemilik akun, serta metode seperti transfer, cek saldo, dan ubah pin.

Identifikasi hubungan antar objek: Analisislah hubungan dan interaksi antara objek-objek yang ada dalam proses bisnis. Ini dapat mencakup hubungan seperti asosiasi, komposisi, pewarisan, dan lain sebagainya. Misalnya, objek Transaksi dapat terkait dengan objek Akun sebagai pengirim dan penerima dalam transfer.

Buat hierarki kelas: Identifikasi kelas-kelas yang diperlukan berdasarkan objek-objek yang telah diidentifikasi dan hubungan antara mereka. Buat hierarki kelas dengan mempertimbangkan pewarisan dan polimorfisme untuk mengelompokkan objek-objek yang berbagi sifat-sifat dan perilaku yang sama. Misalnya, kelas Transaksi bisa menjadi kelas abstrak dengan kelas-kelas turunan seperti Transfer, Pembayaran, dan TopUpPulsa.

Implementasikan kelas dan metode: Implementasikan kelas-kelas yang telah ditentukan dan metode-metode yang berkaitan dengan atribut dan perilaku objek. Pastikan setiap kelas memiliki tanggung jawab yang jelas dan terkait dengan fungsionalitas yang spesifik dalam proses bisnis.

Gunakan objek dalam use case: Gunakan objek-objek yang telah dibuat dalam skenario use case atau alur proses bisnis yang diinginkan. Gunakan metode-metode yang sesuai untuk memanipulasi data dan melakukan interaksi antar objek sesuai dengan langkah-langkah dalam proses bisnis.

Uji dan validasi: Lakukan pengujian untuk memastikan bahwa implementasi OOP memenuhi persyaratan bisnis dan memberikan hasil yang diharapkan. Pastikan objek-objek berperilaku dengan benar, data terkelola dengan baik, dan interaksi antar objek berjalan sesuai kebutuhan.

# NO. 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

![ClassDiagram](https://gitlab.com/1217050064/project-brimo/-/raw/main/ClassDiagram.jpg)

Use Case Table:

| Use Case ID | Use Case Name | Description |
|-------------|---------------|-------------|
|UC-1|Cek Saldo|User dapat mengecek saldo akun mereka|
|UC-2|Transfer|User dapat melakukan transfer ke akun lain|
|UC-3|Proses Transaksi|Transaksi diproses dan dicatat|
|UC-4|Tampil Saldo|Saldo akun ditampilkan di dashboard|
|UC-5|Tampil Transaksi|Transaksi ditampilkan di dashboard|

UC-1: Cek Saldo

Actors: User
Description: User dapat mengecek saldo akun mereka.
Flow of Events:
User membuka aplikasi dan masuk ke dashboard.
User memilih opsi "Cek Saldo".
Sistem menampilkan saldo akun user di dashboard.

UC-2: Transfer

Actors: User
Description: User dapat melakukan transfer ke akun lain.
Flow of Events:
User membuka aplikasi dan masuk ke dashboard.
User memilih opsi "Transfer".
User memasukkan nomor rekening tujuan dan jumlah transfer.
Sistem memvalidasi transaksi dan memperbarui saldo akun pengirim dan penerima.
Sistem mencatat transaksi transfer di dashboard.

UC-3: Proses Transaksi

Actors: -
Description: Transaksi diproses dan dicatat.
Flow of Events:
Sistem menerima data transaksi seperti ID, jumlah, dan tanggal.
Sistem memvalidasi data transaksi.
Jika valid, sistem memproses transaksi dan memperbarui data akun terkait.
Sistem mencatat transaksi dalam catatan transaksi.

UC-4: Tampil Saldo

Actors: User
Description: Saldo akun ditampilkan di dashboard.
Flow of Events:
User membuka aplikasi dan masuk ke dashboard.
Sistem menampilkan saldo akun user di dashboard.

UC-5: Tampil Transaksi

Actors: User
Description: Transaksi ditampilkan di dashboard.
Flow of Events:
User membuka aplikasi dan masuk ke dashboard.
Sistem menampilkan daftar transaksi terakhir user di dashboard.

# NO. 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

![Youtube](https://youtu.be/VRt9Qur0q6I)


# NO. 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

![UX](https://gitlab.com/1217050064/project-brimo/-/raw/main/UX.gif)
