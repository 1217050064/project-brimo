
import java.util.ArrayList;
import java.util.Scanner;

abstract class User {
    private String username;
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public abstract void displayInfo();
}

class Admin extends User {
    public Admin(String username, String password) {
        super(username, password);
    }

    @Override
    public void displayInfo() {
        System.out.println("Admin account");
    }
}

class Customer extends User {
    private String name;
    private int age;

    public Customer(String username, String password, String name, int age) {
        super(username, password);
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    @Override
    public void displayInfo() {
        System.out.println("Customer account");
        System.out.println("Name: " + getName());
        System.out.println("Age: " + getAge());
    }
}

abstract class DashboardMenu {
    protected String menuName;

    public DashboardMenu(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuName() {
        return this.menuName;
    }

    public abstract void displayMenu();
}

class TransferMenu extends DashboardMenu {
    public TransferMenu(String menuName) {
        super(menuName);
    }

    @Override
    public void displayMenu() {
        System.out.println("Transfer menu");
        System.out.println("1. Transfer antar rekening");
        System.out.println("2. Transfer ke bank lain");
        Scanner scanner = new Scanner(System.in);
        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                transferAntarRekening();
                break;

            case 2:
                transferBankLain();
                break;

            default:
                break;
        }
    }

    public void transferAntarRekening() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Transfer antar rekening");
        System.out.print("Masukkan nomor rekening tujuan: ");
        String noRekening = scanner.nextLine();
        System.out.print("Masukkan jumlah uang yang akan dikirim: ");
        int jumlahUang = scanner.nextInt();
        scanner.nextLine(); // membaca karakter newline setelah input jumlah uang

        // TODO: validasi nomor rekening dan jumlah uang
        // ...

        System.out.println("Transfer berhasil dilakukan sebesar " + jumlahUang + " ke nomor rekening " + noRekening);
    }

    public void transferBankLain() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Transfer ke bank lain");
        System.out.print("Masukkan nomor rekening tujuan: ");
        String noRekening = scanner.nextLine();
        System.out.print("Masukkan nama bank tujuan: ");
        String namaBank = scanner.nextLine();
        System.out.print("Masukkan jumlah uang yang akan dikirim: ");
        int jumlahUang = scanner.nextInt();
        scanner.nextLine(); // membaca karakter newline setelah input jumlah uang

        // TODO: validasi nomor rekening, nama bank, dan jumlah uang
        // ...

        System.out.println("Transfer berhasil dilakukan sebesar " + jumlahUang + " ke nomor rekening " + noRekening
                + " di bank " + namaBank);
    }
}

class TopUpPulsaMenu extends DashboardMenu {
    public TopUpPulsaMenu(String menuName) {
        super(menuName);
    }

    @Override
    public void displayMenu() {
        System.out.println("Top up pulsa menu");
        System.out.println("1. Telkomsel");
        System.out.println("2. XL Axiata");
        System.out.println("3. Indosat Ooredoo");
        System.out.println("4. Three");
    }
}

class PembayaranMenu extends DashboardMenu {
    public PembayaranMenu(String menuName) {
        super(menuName);
    }

    @Override
    public void displayMenu() {
        System.out.println("Pembayaran menu");
        System.out.println("1. Listrik");
        System.out.println("2. Air");
        System.out.println("3. Telepon");
    }
}

class BrivaMenu extends DashboardMenu {
    public BrivaMenu(String menuName) {
        super(menuName);
    }

    @Override
    public void displayMenu() {
        System.out.println("BRIVA menu");
        System.out.println("1. Cek tagihan");
        System.out.println("2. Bayar tagihan");
    }
}

class Dashboard {
    private ArrayList<User> users;
    private ArrayList<DashboardMenu> menus;

    public Dashboard() {
        this.users = new ArrayList<User>();
        this.menus = new ArrayList<DashboardMenu>();
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void removeUser(User user) {
        this.users.remove(user);
    }

    public boolean loginUser(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                System.out.println("Login successful!");
                user.displayInfo();
                return true;
            }
        }
        System.out.println("Invalid username or password");
        return false;
    }

    public void addMenu(DashboardMenu menu) {
        this.menus.add(menu);
    }

    public void removeMenu(DashboardMenu menu) {
        this.menus.remove(menu);
    }

    public void displayMenus() {
        System.out.println("Dashboard menus:");
        for (DashboardMenu menu : menus) {
            System.out.println(menu.getMenuName());
        }
    }

    public void displayMenu(String menuName) {
        for (DashboardMenu menu : menus) {
            if (menu.getMenuName().equals(menuName)) {
                menu.displayMenu();
                return;
            }
        }
        System.out.println("Invalid menu name");
    }
}

public class MenuPerbankan {
    public static void main(String[] args) {
        Admin admin = new Admin("admin", "admin");
        Customer customer = new Customer("hilmiali", "123456", "Hilmi Ali", 20);
        Dashboard dashboard = new Dashboard();
        dashboard.addUser(admin);
        dashboard.addUser(customer);

        TransferMenu transferMenu = new TransferMenu("Transfer");
        TopUpPulsaMenu topUpPulsaMenu = new TopUpPulsaMenu("Top up pulsa");
        PembayaranMenu pembayaranMenu = new PembayaranMenu("Pembayaran");
        BrivaMenu brivaMenu = new BrivaMenu("BRIVA");

        dashboard.addMenu(transferMenu);
        dashboard.addMenu(topUpPulsaMenu);
        dashboard.addMenu(pembayaranMenu);
        dashboard.addMenu(brivaMenu);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to the dashboard!");
        System.out.println("Please enter your username and password to login");

        String username = scanner.nextLine();
        String password = scanner.nextLine();

        while (!dashboard.loginUser(username, password)) {
            System.out.println("Please try again");
            username = scanner.nextLine();
            password = scanner.nextLine();
        }

        dashboard.displayMenus();

        System.out.println("Please enter the menu name to display the options");
        String menuName = scanner.nextLine();
        dashboard.displayMenu(menuName);

    }
}

